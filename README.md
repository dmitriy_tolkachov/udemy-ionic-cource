https://www.udemy.com/course/ionic-2-the-practical-guide-to-building-ios-android-apps

Bootstraping:

- `ionic g` - create new elements

Build/Deploy for native device:

- you should install Andriod Studio to be able to build for Andriod platform: https://developer.android.com/studio
- you should use iOS and have XCode installed to be able to build for iOS

Upload a demo:

- we use https://surge.sh/ to deploy a demo
- `ng build` to build the assets
- `surge` to deploy to a static website
- http://ionic-angular-demo-1.surge.sh/

Firebase Console: https://console.firebase.google.com/u/0/project/udemy-ionic-cource/overview

Firebase Realtime DB: https://console.firebase.google.com/u/0/project/udemy-ionic-cource/database/udemy-ionic-cource/data

Postman: https://web.postman.co/build/workspace/My-Workspace~8f9f740c-30eb-406d-84c7-943b3ae1877f/request/128146-370b5fc3-5b1a-421f-aa7b-61ce0870d15b

Test data:

```
[
new Place(
      'Manhattan Mansion',
      'In the heart of New York City.',
      'https://lonelyplanetimages.imgix.net/mastheads/GettyImages-538096543_medium.jpg?sharp=10&vib=20&w=1200',
      149.99,
      new Date('2020-07-01'),
      new Date('2020-12-20'),
      '321',
      'p1'
    ),
    new Place(
      `L'Amour Toujours`,
      'A romantic place in Paris!',
      'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e6/Paris_Night.jpg/1024px-Paris_Night.jpg',
      189.99,
      new Date('2020-09-01'),
      new Date('2020-09-30'),
      '123',
      'p2'
    ),
    new Place(
      'The Foggy Palace',
      'Not your average city trip!',
      'https://upload.wikimedia.org/wikipedia/commons/0/01/San_Francisco_with_two_bridges_and_the_fog.jpg',
      99.99,
      new Date('2020-01-01'),
      new Date('2020-08-30'),
      '123',
      'p3'
    )
]
```

We use firebase functions for backend:

- `npm i -g firebase-tools` to install tools for firebase functions
- `npm run deploy` in the `functions` folder to deploy all functions listed in `index.js` file
- https://console.cloud.google.com/functions/list?folder=&organizationId=&project=udemy-ionic-cource - set public permissions to functions

Additional command:

- "launch:android": "npx cap open android" - to open Andriod Studio without building a project
- `ionic cap open android`
- "run:android": "ionic capacitor run android",
- "build:android": "ionic capacitor build android --prod --release",

Perepare Andriod release:

one time actions:
- set app ID, name in `capacitor.config.json`
- set manifest package to app ID in `android/app/src/main/AndroidManifest.xml`
- set app name in `android/app/src/res/values/strings.xml`
- set app version and app ID in `android/app/build.gradle`
- use https://apetools.webprofusion.com/#/ to generate icons and splash screens
- generate app icons in Android Studio `app/res` right btn click -> new -> Image Asset
- update splash screens in VS Code `android/app/src/main/res`
each build actions:
- set app version and app ID in `android/app/build.gradle`
- generate APK in Android Studio top menu: Build -> Generate Signed Bundle or APK
release
- https://drive.google.com/file/d/1sK0FXzDxIg_zvFMNY9XMVbp2iEf2LN_e/view?usp=sharing
