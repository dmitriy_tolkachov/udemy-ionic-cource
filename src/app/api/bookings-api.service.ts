import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { AuthService } from 'src/app/auth/services/auth.service';
import { BookingDomainModel } from './models/booking.domain-model';
import { Booking } from '../bookings/booking.model';

@Injectable({
    providedIn: 'root'
})
export class BookingsApiService {
    private readonly bookingsCollection = 'bookings';

    constructor(
        private httpClient: HttpClient,
        private authService: AuthService
    ) { }

    private convertDomainModelToBusinessModel(id: string, domainModel: BookingDomainModel): Booking {
        return {
            id: id,
            placeId: domainModel.placeId,
            userId: domainModel.userId,
            guestsNumber: domainModel.guestsNumber,
            leadGuestName: domainModel.leadGuestName,
            placeTitle: domainModel.placeTitle,
            placeImageUrl: domainModel.placeImageUrl,
            startDate: new Date(domainModel.startDate),
            endDate: new Date(domainModel.endDate)
        } as Booking;
    }
    private convertBusinessModelToDomainModel(businessModel: Booking): BookingDomainModel {
        return {
            placeId: businessModel.placeId,
            placeTitle: businessModel.placeTitle,
            placeImageUrl: businessModel.placeImageUrl,
            // TODO: a date becomes "2020-09-26T04:48:07.000Z" but should be "2020-12-20T23:59:00.000Z"
            startDate: businessModel.startDate.toISOString(),
            endDate: businessModel.endDate.toISOString(),
            userId: businessModel.userId,
            guestsNumber: businessModel.guestsNumber,
            leadGuestName: businessModel.leadGuestName,
        } as BookingDomainModel;
    }

    getBookings(): Observable<Booking[]> {
        const queryParams = `orderBy="userId"&equalTo="${this.authService.userID}"`;
        return this.httpClient.get(`${environment.firebaseDbUrl}/${this.bookingsCollection}.json?${queryParams}`).pipe(
            map((res: { [key: string]: BookingDomainModel }) => {
                const bookings: Booking[] = [];
                for (const key in res) {
                    if (res.hasOwnProperty(key)) {
                        bookings.push(this.convertDomainModelToBusinessModel(key, res[key]));
                    }
                }
                return bookings;
            })
        );
    }
    book(booking: Booking): Promise<Booking> {
        const domainModel = this.convertBusinessModelToDomainModel(booking);
        return this.httpClient.post(`${environment.firebaseDbUrl}/${this.bookingsCollection}.json`, domainModel).toPromise()
            .then((res: { name: string }) => {
                booking.id = res.name;
                return booking;
            });
    }
    cancelBooking(id: string): Promise<void> {
        return this.httpClient.delete(`${environment.firebaseDbUrl}/${this.bookingsCollection}/${id}.json`).toPromise()
            .then(res => {
                return;
            });
    }
}
