import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { PlaceDomainModel } from './models/place.domain-model';
import { Place } from '../places/place.model';

@Injectable({
  providedIn: 'root'
})
export class PlacesApiService {
  private readonly placesCollection = 'places';

  constructor(
    private httpClient: HttpClient
  ) { }

  private convertDomainModelToBusinessModel(id: string, domainModel: PlaceDomainModel): Place {
    return {
      id: id,
      description: domainModel.description,
      imageUrl: domainModel.imageUrl,
      price: domainModel.price,
      title: domainModel.title,
      userId: domainModel.userId,
      availableFrom: new Date(domainModel.availableFrom),
      availableTo: new Date(domainModel.availableTo),
      location: {
        address: domainModel.location.address,
        coords: {
          lat: domainModel.location.lat,
          lng: domainModel.location.lng
        }
      }
    } as Place;
  }
  private convertBusinessModelToDomainModel(businessModel: Place): PlaceDomainModel {
    return {
      title: businessModel.title,
      description: businessModel.description,
      price: businessModel.price,
      userId: businessModel.userId,
      imageUrl: businessModel.imageUrl,
      // TODO: a date becomes "2020-09-26T04:48:07.000Z" but should be "2020-12-20T23:59:00.000Z"
      availableFrom: businessModel.availableFrom.toISOString(),
      availableTo: businessModel.availableTo.toISOString(),
      location: {
        address: businessModel.location.address,
        lat: businessModel.location.coords.lat,
        lng: businessModel.location.coords.lng
      }
    };
  }

  getPlaces() {
    return this.httpClient.get(`${environment.firebaseDbUrl}/${this.placesCollection}.json`).pipe(
      map((res: { [key: string]: PlaceDomainModel }) => {
        const places: Place[] = [];
        for (const key in res) {
          if (res.hasOwnProperty(key)) {
            places.push(this.convertDomainModelToBusinessModel(key, res[key]));
          }
        }
        return places;
      })
    );
  }
  getPlace(id: string) {
    return this.httpClient.get(`${environment.firebaseDbUrl}/${this.placesCollection}/${id}.json`).pipe(
      map((res: PlaceDomainModel) => {
        return this.convertDomainModelToBusinessModel(id, res);
      })
    );
  }
  /** TODO: convert business model to domain model */
  createPlace(place: Place): Promise<Place> {
    const placeDomainModel = this.convertBusinessModelToDomainModel(place);
    return this.httpClient.post(`${environment.firebaseDbUrl}/${this.placesCollection}.json`, placeDomainModel).toPromise()
      .then((res: { name: string }) => {
        place.id = res.name;
        return place;
      });
  }
  updatePlace(id: string, place: Place): Promise<Place> {
    const data = this.convertBusinessModelToDomainModel(place);
    return this.httpClient.put(`${environment.firebaseDbUrl}/${this.placesCollection}/${id}.json`, data).toPromise()
      .then(res => {
        return place;
      });
  }
}
