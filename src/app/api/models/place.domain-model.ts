export interface PlaceDomainModel {
    availableFrom: string;
    availableTo: string;
    description: string;
    imageUrl: string;
    price: number;
    title: string;
    userId: string;
    location: {
        address: string;
        lat: number;
        lng: number;
    };
}
