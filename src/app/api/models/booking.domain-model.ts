export interface BookingDomainModel {
    placeId: string;
    userId: string;

    guestsNumber: number;
    leadGuestName: string; // concat first and last name
    startDate: string;
    endDate: string;

    placeTitle: string;
    placeImageUrl: string;
}
