import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ImageService {
    private readonly uploadImageUrl = 'storeImage';

    constructor(
        private httpClient: HttpClient
    ) { }


    private base64toBlob(base64Data: string, contentType: string = ''): Blob {
        const sliceSize = 1024;
        const byteCharacters = window.atob(base64Data);
        const bytesLength = byteCharacters.length;
        const slicesCount = Math.ceil(bytesLength / sliceSize);
        const byteArrays = new Array(slicesCount);

        for (let sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
            const begin = sliceIndex * sliceSize;
            const end = Math.min(begin + sliceSize, bytesLength);

            const bytes = new Array(end - begin);
            for (let offset = begin, i = 0; offset < end; ++i, ++offset) {
                bytes[i] = byteCharacters[offset].charCodeAt(0);
            }
            byteArrays[sliceIndex] = new Uint8Array(bytes);
        }
        return new Blob(byteArrays, { type: contentType });
    }
    /** e.g. image/jpeg */
    private getImageContentType(imageData: string): string {
        return imageData.split(';')[0].split(':')[1];
    }
    convertImageStringToFile(imageData: string): Blob {
        const imageContentType = this.getImageContentType(imageData);
        const updatedImageData = imageData.replace(`data:${imageContentType};base64,`, '');
        const imageFile = this.base64toBlob(
            updatedImageData,
            imageContentType
        );
        return imageFile;
    }

    uploadImage(image: Blob): Promise<{ imageUrl: string, imagePath: string }> {
        const uploadData = new FormData();
        uploadData.append('image', image);
        return this.httpClient.post(`${environment.firebaseFunctions}/${this.uploadImageUrl}`, uploadData).toPromise()
            .then((res: any) => {
                console.log('image uploaded', res);
                return res;
            });
    }
}
