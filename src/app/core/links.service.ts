import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LinksService {

  constructor() { }

  getPlacesDiscoverLink() {
    return `/places/tabs/discover`;
  }
  getPlaceDetailsLink(placeId: string) {
    return `/places/tabs/discover/${placeId}`;
  }

  getOffersLink() {
    return `/places/tabs/offers`;
  }
  getNewOfferLink() {
    return '/places/tabs/offers/new';
  }
  getEditOfferLink(placeId: string) {
    return `/places/tabs/offers/edit/${placeId}`;
  }

  getBookingsLink() {
    return '/bookings';
  }

  getAuthenticateLink() {
    return '/auth';
  }
}
