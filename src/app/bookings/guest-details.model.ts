export class GuestDetails {
    constructor(
        public firstName: string,
        public lastName: string,
        public guestsNumber: number,
        public startDate: Date,
        public endDate: Date
    ) { }
}
