import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { AuthService } from 'src/app/auth/services/auth.service';
import { Place } from 'src/app/places/place.model';
import { GuestDetails } from './guest-details.model';
import { BookingsApiService } from './../api/bookings-api.service';
import { Booking } from './booking.model';

@Injectable({
  providedIn: 'root'
})
export class BookingsService {
  private bookings: Booking[] = [];
  private bookings$ = new BehaviorSubject(this.bookings); // new BehaviorSubject([...this.bookings]);

  constructor(
    private authService: AuthService,
    private bookingApiService: BookingsApiService
  ) { }

  /** load bookings from server */
  loadBookings(): Promise<Booking[]> {
    return this.bookingApiService.getBookings().toPromise().then(bookings => {
      this.bookings = bookings;
      this.bookings$.next(this.bookings);
      return this.bookings;
    });
  }
  getBookings(): Observable<Booking[]> {
    return this.bookings$ as Observable<Booking[]>;
  }

  async book(place: Place, guestDetails: GuestDetails): Promise<Booking> {
    let booking: Booking = {
      guestsNumber: guestDetails.guestsNumber,
      leadGuestName: `${guestDetails.firstName} ${guestDetails.lastName}`,
      placeId: place.id,
      placeTitle: place.title,
      placeImageUrl: place.imageUrl,
      startDate: guestDetails.startDate,
      endDate: guestDetails.endDate,
      userId: this.authService.userID
    };
    booking = await this.bookingApiService.book(booking);

    this.bookings.push(booking);
    this.bookings$.next(this.bookings);

    return booking;
  }

  cancelBooking(id: string): Promise<void> {
    return this.bookingApiService.cancelBooking(id).then(() => {
      this.bookings = this.bookings.filter(b => b.id !== id);
      this.bookings$.next(this.bookings);
    });
  }
}
