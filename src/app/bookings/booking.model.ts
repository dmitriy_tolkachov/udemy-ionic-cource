export class Booking {
    constructor(
        public placeId: string,
        public userId: string,
        public guestsNumber: number,
        public leadGuestName: string, // concat first and last name
        public startDate: Date,
        public endDate: Date,

        public placeTitle: string,
        public placeImageUrl: string,

        public id?: string // if not set - bookig is not saved to DB
    ) { }
}
