import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BookingsPageRoutingModule } from './bookings-routing.module';
import { BookingsPage } from './bookings.page';
import { CreateBookingModalComponent } from './create-booking-modal/create-booking-modal.component';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    ReactiveFormsModule,

    BookingsPageRoutingModule
  ],
  declarations: [
    BookingsPage,
    CreateBookingModalComponent
  ],
  exports: [
    CreateBookingModalComponent
  ],
  entryComponents: [
    CreateBookingModalComponent
  ]
})
export class BookingsPageModule { }
