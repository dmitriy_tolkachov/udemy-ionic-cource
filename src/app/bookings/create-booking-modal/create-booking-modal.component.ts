import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

import { GuestDetails } from '../guest-details.model';
import { Place } from 'src/app/places/place.model';

@Component({
  selector: 'app-create-booking-modal',
  templateUrl: './create-booking-modal.component.html',
  styleUrls: ['./create-booking-modal.component.scss'],
})
export class CreateBookingModalComponent implements OnInit {
  @Input() place: Place;
  @Input() isRandomDate: boolean;

  form: FormGroup;
  readonly dateFormat = 'DD MMM YYYY';

  constructor(
    private modalController: ModalController
  ) { }

  ngOnInit() {
    this.initForm();
  }

  /** Date -> 'DD MMM YYYY' */
  private getDateStrForDatepicker(date: Date): string {
    const fragments = date.toUTCString().split(' ');
    return `${fragments[1]} ${fragments[2]} ${fragments[3]}`;
  }
  private initForm() {
    let defaultStartDate: string;
    let defaultEndDate: string;
    // TODO: validate start & end date
    // TODO: add restrictions to date pickers
    if (this.isRandomDate) {
      const now = new Date();
      defaultStartDate = this.getDateStrForDatepicker(new Date(now.setDate(now.getDate() + 1)));
      defaultEndDate = this.getDateStrForDatepicker(new Date(now.setDate(now.getDate() + 7)));
    }

    this.form = new FormGroup({
      firstName: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(1), Validators.maxLength(100)]
      }),
      lastName: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(2), Validators.maxLength(100)]
      }),
      guestsNumber: new FormControl('2', {
        validators: [Validators.required, Validators.min(1), Validators.max(10)]
      }),
      startDate: new FormControl(defaultStartDate, {
        validators: [Validators.required]
      }),
      endDate: new FormControl(defaultEndDate, {
        validators: [Validators.required]
      })
    });
  }

  close() {
    this.modalController.dismiss(undefined, 'cancel');
  }
  bookPlace() {
    const guestDetails: GuestDetails = {
      firstName: this.form.value.firstName,
      lastName: this.form.value.lastName,
      guestsNumber: +this.form.value.guestsNumber,
      startDate: new Date(this.form.value.startDate),
      endDate: new Date(this.form.value.endDate)
    };
    this.modalController.dismiss(guestDetails, 'confirm');
  }

  isFormControlInvalid(formControlName: string) {
    const formControl = this.form.get(formControlName);
    return formControl.invalid && formControl.touched;
  }
}
