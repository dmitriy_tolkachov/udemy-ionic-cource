import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { NavController, LoadingController } from '@ionic/angular';

import { LinksService } from 'src/app/core/links.service';
import { BookingsService } from './bookings.service';
import { Booking } from './booking.model';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.page.html',
  styleUrls: ['./bookings.page.scss'],
})
export class BookingsPage implements OnInit, OnDestroy {
  defaultBackUrl: string;
  bookings: Booking[];

  private bookingsSub: Subscription;
  private loadingOverlay: HTMLIonLoadingElement;

  constructor(
    private linksService: LinksService,
    private navController: NavController,
    private loadingController: LoadingController,
    private bookingsService: BookingsService
  ) { }

  ngOnInit() {
    this.defaultBackUrl = this.linksService.getPlacesDiscoverLink();
    this.bookingsSub = this.bookingsService.getBookings().subscribe(bookings => {
      this.bookings = bookings;
    });
    this.bookingsService.loadBookings();
  }
  ngOnDestroy() {
    if (this.bookingsSub) {
      this.bookingsSub.unsubscribe();
    }
  }

  discoverPlaces() {
    this.navController.navigateForward(this.linksService.getPlacesDiscoverLink());
  }

  async cancelBooking(id: string) {
    await this.showLoadingOverlay();
    await this.bookingsService.cancelBooking(id);
    await this.hideLoadingOverlay();
  }

  private async showLoadingOverlay() {
    this.loadingOverlay = await this.loadingController.create({
      keyboardClose: true,
      message: 'Cancelling your booking...'
    });
    return this.loadingOverlay.present();
  }
  private hideLoadingOverlay() {
    return this.loadingOverlay.dismiss();
  }
}
