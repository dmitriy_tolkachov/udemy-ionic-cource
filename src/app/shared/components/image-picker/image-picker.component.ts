import { AlertController } from '@ionic/angular';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CameraResultType, CameraSource, Plugins } from '@capacitor/core';

type View = 'image-selected' | 'image-not-selected';

@Component({
  selector: 'app-image-picker',
  templateUrl: './image-picker.component.html',
  styleUrls: ['./image-picker.component.scss'],
})
export class ImagePickerComponent implements OnInit {
  @Output() imageChange = new EventEmitter<string>();
  @Input() image: string;
  view: View;
  imageUrl: string;

  constructor(
    private alertController: AlertController
  ) { }

  ngOnInit() {
    this.view = 'image-not-selected';
  }

  private showAlert(msg: string, title?: string) {
    this.alertController.create({
      header: title,
      message: msg,
      buttons: [
        { text: 'Ok' }
      ]
    }).then(el => el.present());
  }

  async showActionSheet() {
    try {
      const photo = await Plugins.Camera.getPhoto({
        source: CameraSource.Prompt,
        quality: 90,
        correctOrientation: true,
        width: 600,
        resultType: CameraResultType.DataUrl
      });

      this.imageUrl = photo.dataUrl;
      this.view = 'image-selected';

      this.image = photo.dataUrl;
      this.imageChange.emit(this.image);
    } catch (err) {
      this.showAlert(`Can't take a picture.`);
    }
  }
}
