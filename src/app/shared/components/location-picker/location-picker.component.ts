import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ModalController, ActionSheetController, AlertController } from '@ionic/angular';
import { Plugins } from '@capacitor/core';

import { MapModalComponent } from '../map-modal/map-modal.component';
import { GoogleMapsService } from './../../services/google-maps.service';
import { Coords } from './../../models/coordinates.model';
import { PlaceLocation } from './../../models/place-location.model';

const { Geolocation } = Plugins;
type View = 'location-selected' | 'location-not-selected';

@Component({
  selector: 'app-location-picker',
  templateUrl: './location-picker.component.html',
  styleUrls: ['./location-picker.component.scss'],
})
export class LocationPickerComponent implements OnInit {
  @Output() locationChange = new EventEmitter<PlaceLocation>();
  @Input() location: PlaceLocation;

  view: View;
  pickedAddress: string;
  staticMapUrl: string;
  private pickedCoords: Coords;

  constructor(
    private actionSheetController: ActionSheetController,
    private alertController: AlertController,
    private modalController: ModalController,
    private googleMapsService: GoogleMapsService
  ) { }

  ngOnInit() {
    if (this.location) {
      this.pickedAddress = this.location.address;
      this.pickedCoords = this.location.coords;
      this.staticMapUrl = this.googleMapsService.getStaticMap(this.pickedCoords);
      this.view = 'location-selected';
    } else {
      this.view = 'location-not-selected';
    }
  }

  private async showAlert(msg: string, title?: string) {
    const el = await this.alertController.create({
      header: title,
      message: msg,
      buttons: [
        { text: 'Ok' }
      ]
    });
    await el.present();
  }

  async showActionSheet() {
    const actionSheetEl = await this.actionSheetController.create({
      header: 'Pick a location',
      buttons: [{
        text: 'Current Location',
        handler: () => {
          this.getCurrentPosition();
        }
      }, {
        text: 'Pick on Map',
        handler: () => {
          this.pickLocation();
        }
      }, {
        text: 'Cancel',
        role: 'cancel'
      }]
    });
    await actionSheetEl.present();
  }

  private pickLocation() {
    this.modalController.create({
      component: MapModalComponent,
      componentProps: {
        center: this.pickedCoords
      }
    }).then((modalEl) => {
      modalEl.onDidDismiss().then(res => {
        this.pickedCoords = res.data;
        return this.googleMapsService.getAddress(res.data);
      }).then(address => {
        this.pickedAddress = address;
        this.staticMapUrl = this.googleMapsService.getStaticMap(this.pickedCoords);
        this.view = 'location-selected';

        this.location = {
          address: this.pickedAddress,
          coords: this.pickedCoords
        };
        this.locationChange.emit(this.location);
      });
      modalEl.present();
    });
  }

  private async getCurrentPosition() {
    try {
      const coordinates = await Geolocation.getCurrentPosition();
      this.pickedCoords = {
        lat: coordinates.coords.latitude,
        lng: coordinates.coords.longitude
      };
      this.staticMapUrl = this.googleMapsService.getStaticMap(this.pickedCoords);
      this.pickedAddress = await this.googleMapsService.getAddress(this.pickedCoords);
      this.view = 'location-selected';

      this.location = {
        address: this.pickedAddress,
        coords: this.pickedCoords
      };
      this.locationChange.emit(this.location);
    } catch (err) {
      this.showAlert('Please, pick a location on map.', `Can't get current location.`);
    }
  }
}
