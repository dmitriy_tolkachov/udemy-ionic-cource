import { NavController } from '@ionic/angular';
import { Component, OnInit, Input } from '@angular/core';

import { LinksService } from 'src/app/core/links.service';
import { AuthService } from 'src/app/auth/services/auth.service';

@Component({
  selector: 'app-side-drawer',
  templateUrl: './side-drawer.component.html',
  styleUrls: ['./side-drawer.component.scss'],
})
export class SideDrawerComponent implements OnInit {
  @Input() contentId: string;
  @Input() menuId: string;

  bookingsLink: string;
  discoverPlacesLink: string;
  isAuthenticated: boolean;

  constructor(
    private linksService: LinksService,
    private authService: AuthService,
    private navController: NavController
  ) { }

  ngOnInit() {
    this.discoverPlacesLink = this.linksService.getPlacesDiscoverLink();
    this.bookingsLink = this.linksService.getBookingsLink();
  }

  async onMenuOpen() {
    await this.authService.restoreAuthData(); // can be implemented as observable
    this.isAuthenticated = this.authService.authenticated;
  }

  logout() {
    this.authService.logout();
    this.isAuthenticated = this.authService.authenticated;

    // TODO: improve to check if page protected: if so, navigate to login page, stay on a current page otherwise
    this.navController.navigateForward(this.linksService.getAuthenticateLink());
  }
  login() {
    this.navController.navigateForward(this.linksService.getAuthenticateLink());
  }
}
