import { ModalController, AlertController } from '@ionic/angular';
import { AfterViewInit, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';

import { GoogleMapsService } from './../../services/google-maps.service';
import { Coords } from './../../models/coordinates.model';

@Component({
  selector: 'app-map-modal',
  templateUrl: './map-modal.component.html',
  styleUrls: ['./map-modal.component.scss'],
})
export class MapModalComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() clickable: boolean;
  @Input() center: Coords;
  @ViewChild('map') mapElement: ElementRef;
  private mapClickListener: any;

  constructor(
    private modalController: ModalController,
    private alertController: AlertController,
    private googleMapsService: GoogleMapsService
  ) {
    this.clickable = true;
  }

  ngOnInit() { }

  ngAfterViewInit() {
    const mapEl = this.mapElement.nativeElement;
    this.googleMapsService.renderMap(mapEl, this.center).then(map => {
      if (this.clickable) {
        this.mapClickListener = this.googleMapsService.addClickListener(map, (coords) => {
          this.modalController.dismiss(coords);
        });
      }
      if (this.center) {
        this.googleMapsService.addMarker(map, this.center);
      }
    }).catch(err => {
      this.showAlert('Google Maps is not available.', 'Error');
    });
  }

  ngOnDestroy() {
    if (this.clickable) {
      this.googleMapsService.removeClickListener(this.mapClickListener);
    }
  }

  cancel() {
    this.modalController.dismiss();
  }

  private showAlert(msg: string, title?: string) {
    this.alertController.create({
      header: title,
      message: msg,
      buttons: [
        {
          text: 'Ok'
        }
      ]
    }).then(alertEl => alertEl.present());
  }
}
