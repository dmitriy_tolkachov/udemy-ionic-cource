import { Coords } from './coordinates.model';

export class PlaceLocation {
    coords: Coords;
    address: string;
}
