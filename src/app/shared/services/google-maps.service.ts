import { HttpClient } from '@angular/common/http';
import { Injectable, RendererFactory2 } from '@angular/core';

import { environment } from 'src/environments/environment';
import { Coords } from './../models/coordinates.model';

@Injectable({
  providedIn: 'root'
})
export class GoogleMapsService {
  private readonly googleMapsScriptUrl = 'https://maps.googleapis.com/maps/api/js';
  private readonly googleMapsGeocoding = 'https://maps.googleapis.com/maps/api/geocode/json';
  private readonly googleMapsStatic = `https://maps.googleapis.com/maps/api/staticmap`;

  constructor(
    // @Inject(DOCUMENT) private document
    private httpClient: HttpClient,
    private rendererFactory: RendererFactory2
  ) { }

  private get googleMaps(): Promise<any> {
    const googleModule = (window as any).google;
    if (googleModule && googleModule.maps) {
      return Promise.resolve(googleModule.maps);
    } else {
      // load google maps script
      return new Promise((resolve, reject) => {
        const script = document.createElement('script');
        script.src = `${this.googleMapsScriptUrl}?key=${environment.googleMapsKey}`;
        script.async = true;
        script.defer = true;
        script.onload = () => {
          const loadedGoogleModule = (window as any).google;
          if (loadedGoogleModule && loadedGoogleModule.maps) {
            resolve(loadedGoogleModule.maps);
          } else {
            reject('google_maps_not_loaded');
          }
        };
        document.body.appendChild(script);
      });
    }
  }

  /** returns Google Map object */
  renderMap(mapEl: any, center?: Coords): Promise<any> {
    center = center || {
      lat: -33.8607772,
      lng: 151.20792
    }; // default center

    return this.googleMaps.then(googleMaps => {
      const map = new googleMaps.Map(mapEl, {
        center: {
          lat: center.lat,
          lng: center.lng
        },
        zoom: 16
      });

      // add css class to indicate that map was loaded
      googleMaps.event.addListenerOnce(map, 'idle', () => {
        // https://stackoverflow.com/questions/44989666/service-no-provider-for-renderer2
        this.rendererFactory.createRenderer(null, null).addClass(mapEl, 'visible');
      });

      return map;
    }).catch(err => {
      return err;
    });
  }
  /** returns google maps event listener */
  addClickListener(map: any, callback: (coords: Coords) => void): any {
    return map.addListener('click', event => {
      const selectedLocation: Coords = {
        lat: event.latLng.lat(),
        lng: event.latLng.lng()
      };
      callback(selectedLocation);
    });
  }
  async removeClickListener(clickListener: any) {
    if (clickListener) {
      (await this.googleMaps).event.removeListener(clickListener);
    }
  }

  getAddress(coords: Coords): Promise<string> {
    return this.httpClient.get(`${this.googleMapsGeocoding}?latlng=${coords.lat},${coords.lng}&key=${environment.googleMapsKey}`)
      .toPromise()
      .then((res: any) => {
        return res.results ? res.results[0].formatted_address : undefined;
      }).catch(err => {
        return err;
      });
  }

  getStaticMap(coords: Coords): string {
    return `${this.googleMapsStatic}?center=${coords.lat},${coords.lng}
    &zoom=16
    &size=500x300
    &maptype=roadmap
    &markers=color:red%7Clabel:Place%7C${coords.lat},${coords.lng}
    &key=${environment.googleMapsKey}`;
  }

  async addMarker(map: any, coords: Coords) {
    return new (await this.googleMaps).Marker({
      position: {
        lat: coords.lat,
        lng: coords.lng
      },
      map: map
    });
  }
}
