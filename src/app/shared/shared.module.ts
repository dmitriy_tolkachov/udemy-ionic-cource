import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouteReuseStrategy, RouterModule } from '@angular/router';

import { IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { SideDrawerComponent } from './components/side-drawer/side-drawer.component';
import { LocationPickerComponent } from './components/location-picker/location-picker.component';
import { ImagePickerComponent } from './components/image-picker/image-picker.component';
import { MapModalComponent } from './components/map-modal/map-modal.component';

@NgModule({
  declarations: [
    SideDrawerComponent,
    LocationPickerComponent,
    ImagePickerComponent,
    MapModalComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    IonicModule.forRoot(),
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  exports: [
    SideDrawerComponent,
    LocationPickerComponent,
    ImagePickerComponent,
    MapModalComponent,
    CommonModule,
    IonicModule
  ],
  entryComponents: [
    MapModalComponent
  ]
})
export class SharedModule { }
