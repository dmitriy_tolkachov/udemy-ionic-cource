export interface AuthResponse {
    email: string;
    // TODO: convert to number of seconds
    expiresIn: string;
    idToken: string;
    kind: string; // "identitytoolkit#SignupNewUserResponse"
    localId: string;
    refreshToken: string;
}
