export interface AuthStorageModel {
    UID: string;
    email: string;
    token: string;
    tokenExpirationTime: Date;
}
