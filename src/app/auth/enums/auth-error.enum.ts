export enum AuthError {
    // login
    EmailNotFound = 'EMAIL_NOT_FOUND',
    InvalidPassword = 'INVALID_PASSWORD',
    UserDisabled = 'USER_DISABLED',
    // signup
    EmailExists = 'EMAIL_EXISTS',
    OperationForbidden = 'OPERATION_NOT_ALLOWED',
    TooManyRequests = 'TOO_MANY_ATTEMPTS_TRY_LATER'
}
