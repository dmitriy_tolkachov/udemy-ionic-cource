import { Injectable } from '@angular/core';
import { CanLoad, Route, UrlSegment, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { LinksService } from 'src/app/core/links.service';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad {
  constructor(
    private authService: AuthService,
    private linksService: LinksService,
    private router: Router
  ) { }
  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {

    if (!this.authService.authenticated) {
      this.router.navigateByUrl(this.linksService.getAuthenticateLink());
      return false;

      // // try to auto-login
      // this.authService.restoreAuthData().then(() => {
      //   if (!this.authService.authenticated) {
      //     this.router.navigateByUrl(this.linksService.getAuthenticateLink());
      //     return false;
      //   }
      //   return true;
      // });
    }
    return true;
  }
}
