import { AuthStorageModel } from './../models/auth-storage.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Plugins } from '@capacitor/core';

import { environment } from 'src/environments/environment';
import { AuthRequest } from '../models/auth-request.model';
import { AuthResponse } from '../models/auth-response.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly signupEndpoint = 'v1/accounts:signUp';
  private readonly signinEndpoint = 'v1/accounts:signInWithPassword';
  private readonly authStorageKey = 'authData';

  // auth data
  private uid: string;
  private email: string;
  private idToken: string;
  private idTokenExpirationTime: Date;

  constructor(
    private httpClient: HttpClient
  ) { }

  get authenticated() {
    return this.idToken && this.idTokenExpirationTime.getTime() > Date.now();
  }
  get userID() {
    return this.uid;
  }

  private async storeAuthData() {
    const data: AuthStorageModel = {
      UID: this.uid,
      email: this.email,
      token: this.idToken,
      tokenExpirationTime: this.idTokenExpirationTime
    };
    await Plugins.Storage.set({
      key: this.authStorageKey,
      value: JSON.stringify(data)
    });
  }
  async restoreAuthData() {
    const dataStr = await Plugins.Storage.get({
      key: this.authStorageKey
    });
    const data = JSON.parse(dataStr.value) as AuthStorageModel;
    if (data) {
      this.uid = data.UID;
      this.email = data.email;
      this.idToken = data.token;
      this.idTokenExpirationTime = new Date(data.tokenExpirationTime);
    }
  }
  private async cleanAuthData() {
    await Plugins.Storage.remove({
      key: this.authStorageKey
    });
  }
  private setAuthData(res: AuthResponse) {
    this.uid = res.localId;
    this.email = res.email;
    this.idToken = res.idToken;
    this.idTokenExpirationTime = new Date(Date.now() + +res.expiresIn * 1000);
  }
  /**
   * https://firebase.google.com/docs/reference/rest/auth#section-sign-in-email-password
   */
  login(email: string, password: string) {
    return this.httpClient.post(`${environment.firebaseAuthAPI}/${this.signinEndpoint}?key=${environment.firebaseKey}`,
      new AuthRequest(email, password)).toPromise().then((res: AuthResponse) => {
        this.setAuthData(res);
        return this.storeAuthData();
      }).catch(err => {
        throw new Error(err.error.error.message);
      });
  }
  /**
   * https://firebase.google.com/docs/reference/rest/auth#section-create-email-password
   */
  signup(email: string, password: string) {
    return this.httpClient.post(`${environment.firebaseAuthAPI}/${this.signupEndpoint}?key=${environment.firebaseKey}`,
      new AuthRequest(email, password)).toPromise().then((res: AuthResponse) => {
        this.setAuthData(res);
        return this.storeAuthData();
      }).catch(err => {
        throw new Error(err.error.error.message);
      });
  }
  logout() {
    this.uid = undefined;
    this.email = undefined;
    this.idToken = undefined;
    this.idTokenExpirationTime = undefined;
    this.cleanAuthData();
  }
}
