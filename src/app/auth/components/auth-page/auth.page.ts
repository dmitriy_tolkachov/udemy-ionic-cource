import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { LoadingController, AlertController } from '@ionic/angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { LinksService } from 'src/app/core/links.service';
import { AuthService } from 'src/app/auth/services/auth.service';
import { AuthError } from 'src/app/auth/enums/auth-error.enum';

type View = 'login' | 'signup';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss'],
})
export class AuthPage implements OnInit {
  isProcessing = false;
  form: FormGroup;
  view: View;

  private loadingOverlay: HTMLIonLoadingElement;

  constructor(
    private authService: AuthService,
    private linksService: LinksService,
    private loadingController: LoadingController,
    private alertController: AlertController,
    private router: Router // we not imported Ionic module to use NavController
  ) { }

  ngOnInit() {
    this.switchView('login');
    this.initForm();
  }

  private async showLoadingOverlay() {
    this.loadingOverlay = await this.loadingController.create({
      keyboardClose: true,
      message: 'Please wait...'
    });
    this.loadingOverlay.present();
  }
  private hideLoadingOverlay() {
    if (this.loadingOverlay) {
      this.loadingOverlay.dismiss();
    }
  }
  private async showAlert(msg: string) {
    const alert = await this.alertController.create({
      header: 'Oops!',
      message: msg,
      buttons: ['Okay']
    });
    await alert.present();
  }
  private initForm() {
    this.form = new FormGroup({
      email: new FormControl(undefined, { updateOn: 'change', validators: [Validators.email, Validators.required] }),
      password: new FormControl(undefined, { updateOn: 'change', validators: [Validators.minLength(6), Validators.required] })
    });
  }

  switchView(view: View) {
    this.view = view;
  }

  isFormControlInvalid(formControlName: string) {
    const formControl = this.form.get(formControlName);
    return formControl.invalid && formControl.touched;
  }

  authorize() {
    if (!this.form.valid) {
      return;
    }

    this.isProcessing = true;
    this.showLoadingOverlay();
    const authPromise = this.view === 'login' ?
      this.authService.login(this.form.value.email, this.form.value.password) :
      this.authService.signup(this.form.value.email, this.form.value.password);
    authPromise.then(res => {
      this.router.navigateByUrl(this.linksService.getPlacesDiscoverLink());
    }).catch((err: Error) => {
      switch (err.message as AuthError) {
        case AuthError.EmailExists:
          this.showAlert('This email address is already registered. Please try another email address to register.');
          break;
        case AuthError.EmailNotFound:
          this.showAlert('No such email. Please signup first.');
          break;
        case AuthError.InvalidPassword:
          this.showAlert('Please check your password.');
          break;
        default:
          this.showAlert('An error happened. Please try again later or use different email/password.');
      }
    }).finally(() => {
      this.isProcessing = false;
      this.hideLoadingOverlay();
    });
  }
}
