import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BookingsPageModule } from './../bookings/bookings.module';
import { PlacesPageRoutingModule } from './places-routing.module';
import { PlacesPage } from './places.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule, // do we need this?
    IonicModule,

    PlacesPageRoutingModule,
    BookingsPageModule
  ],
  declarations: [PlacesPage]
})
export class PlacesPageModule { }
