import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Place } from '../../place.model';

@Component({
  selector: 'app-place-secondary',
  templateUrl: './place-secondary.component.html',
  styleUrls: ['./place-secondary.component.scss'],
})
export class PlaceSecondaryComponent implements OnInit {
  @Input() place: Place;
  @Output() cta = new EventEmitter<Place>();

  constructor() { }

  ngOnInit() { }

  selectPlace() {
    if (this.cta) {
      this.cta.emit(this.place);
    }
  }
}
