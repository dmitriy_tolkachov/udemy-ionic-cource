import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { NavController, ModalController, ActionSheetController, LoadingController, AlertController } from '@ionic/angular';

import { MapModalComponent } from 'src/app/shared/components/map-modal/map-modal.component';
import { GoogleMapsService } from 'src/app/shared/services/google-maps.service';
import { LinksService } from 'src/app/core/links.service';
import { GuestDetails } from 'src/app/bookings/guest-details.model';
import { CreateBookingModalComponent } from 'src/app/bookings/create-booking-modal/create-booking-modal.component';
import { PlacesService } from 'src/app/places/places.service';
import { BookingsService } from 'src/app/bookings/bookings.service';
import { Place } from './../../place.model';

type View = 'loading-data' | 'data-loaded';

@Component({
  selector: 'app-place-details',
  templateUrl: './place-details.page.html',
  styleUrls: ['./place-details.page.scss'],
})
export class PlaceDetailsPage implements OnInit, OnDestroy {
  place: Place;
  defaultBackUrl: string;
  view: View;
  canBook: boolean;

  private readonly PLACE_ID_PARAM = 'placeId'; // should as in a route
  private paramsSubscription: Subscription;
  private placeSubscription: Subscription;
  private actionSheet: HTMLIonActionSheetElement;
  private loadingOverlay: HTMLIonLoadingElement;

  constructor(
    private linksService: LinksService,
    private placesService: PlacesService,
    private bookingsService: BookingsService,
    private navController: NavController,
    private loadingController: LoadingController,
    private alertController: AlertController,
    private modalController: ModalController,
    public actionSheetController: ActionSheetController,
    private activatedRoute: ActivatedRoute,
    private googleMapsService: GoogleMapsService
  ) { }

  ngOnInit() {
    this.view = 'loading-data';
    this.defaultBackUrl = this.linksService.getPlacesDiscoverLink();
    this.paramsSubscription = this.activatedRoute.paramMap.subscribe(params => {
      if (params.has(this.PLACE_ID_PARAM)) {
        this.placeSubscription = this.placesService.getPlace(params.get(this.PLACE_ID_PARAM))
          .subscribe(
            place => {
              this.place = place;
              this.view = 'data-loaded';
              this.canBook = this.placesService.canBook(this.place);
            },
            error => { this.showCriticalError(`Can't find such place.`); }
          );
      } else {
        this.navController.navigateBack(this.linksService.getPlacesDiscoverLink());
      }
    });
  }
  ngOnDestroy() {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
    if (this.placeSubscription) {
      this.placeSubscription.unsubscribe();
    }
  }

  private async showActionSheet() {
    this.actionSheet = await this.actionSheetController.create({
      header: 'Choose date',
      buttons: [
        { text: 'Select Date', handler: () => { this.showBookingPopup(); } },
        { text: 'Random Date', handler: () => { this.showBookingPopup(true); } },
        {
          text: 'Cancel',
          icon: 'close',
          role: 'cancel'
        }
      ]
    });
    this.actionSheet.present();
  }
  private hideActionSheet() {
    this.actionSheet.dismiss();
  }
  private async showLoadingOverlay() {
    this.loadingOverlay = await this.loadingController.create({
      keyboardClose: true,
      message: 'Booking a place for you...'
    });
    return this.loadingOverlay.present();
  }
  private hideLoadingOverlay() {
    return this.loadingOverlay.dismiss();
  }
  private async showBookingPopup(isRandomDate: boolean = false) {
    const modal = await this.modalController.create({
      component: CreateBookingModalComponent,
      componentProps: {
        place: this.place,
        isRandomDate: isRandomDate
      }
    });
    await modal.present();
    const res = await modal.onDidDismiss();
    const guestDetails = res.data ? res.data as GuestDetails : undefined;

    if (guestDetails) {
      await this.showLoadingOverlay();
      await this.bookingsService.book(this.place, guestDetails);
      await this.hideLoadingOverlay();
      await this.navController.navigateForward(this.linksService.getBookingsLink());
    }
  }
  private showCriticalError(msg: string) {
    return this.alertController.create({
      header: 'Error',
      message: msg || 'Critical error has occured.',
      buttons: [{
        text: 'Got it',
        handler: () => {
          this.navController.navigateBack(this.linksService.getPlacesDiscoverLink());
        }
      }]
    }).then(el => el.present());
  }

  async bookPlace() {
    await this.showActionSheet();
  }
  getStaticMap(): string {
    if (!this.place || !this.place.location || !this.place.location.coords) {
      return;
    }
    return this.googleMapsService.getStaticMap(this.place.location.coords);
  }

  observeLocation() {
    this.modalController.create({
      component: MapModalComponent,
      componentProps: {
        center: this.place.location.coords,
        clickable: false
      }
    }).then(modalEl => modalEl.present());
  }
}
