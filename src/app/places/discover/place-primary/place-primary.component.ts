import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Place } from '../../place.model';

@Component({
  selector: 'app-place-primary',
  templateUrl: './place-primary.component.html',
  styleUrls: ['./place-primary.component.scss'],
})
export class PlacePrimaryComponent implements OnInit {
  @Input() place: Place;
  @Output() cta = new EventEmitter<Place>();

  constructor() { }

  ngOnInit() { }

  selecetPlace() {
    if (this.cta) {
      this.cta.emit(this.place);
    }
  }
}
