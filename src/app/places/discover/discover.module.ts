import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DiscoverPageRoutingModule } from './discover-routing.module';

import { DiscoverPage } from './discover.page';
import { PlacePrimaryComponent } from './place-primary/place-primary.component';
import { PlaceSecondaryComponent } from './place-secondary/place-secondary.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiscoverPageRoutingModule
  ],
  declarations: [
    DiscoverPage,
    PlacePrimaryComponent,
    PlaceSecondaryComponent
  ]
})
export class DiscoverPageModule { }
