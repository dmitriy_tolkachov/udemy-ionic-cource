import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Subscription } from 'rxjs';

import { LinksService } from 'src/app/core/links.service';
import { PlacesService } from 'src/app/places/places.service';
import { Place } from '../place.model';

type View = 'loading-data' | 'no-data' | 'data-loaded';
type PlaceFilter = 'all' | 'bookable';

@Component({
  selector: 'app-discover',
  templateUrl: './discover.page.html',
  styleUrls: ['./discover.page.scss'],
})
export class DiscoverPage implements OnInit, OnDestroy {
  private placesToDiscover: Place[];
  private placesSubscription: Subscription;

  view: View;
  currentFilter: PlaceFilter;
  primaryPlace: Place;
  secondaryPlaces: Place[];

  constructor(
    private linksService: LinksService,
    private placesService: PlacesService,
    private navConteoller: NavController
  ) { }

  ngOnInit() {
    this.currentFilter = 'all';
    this.placesSubscription = this.placesService.getPlaces().subscribe(places => {
      this.placesToDiscover = places;
      this.filterPlaces(this.currentFilter);
    });
  }
  ngOnDestroy() {
    if (this.placesSubscription) {
      this.placesSubscription.unsubscribe();
    }
  }

  // TODO: implement as on bookings page
  /** https://ionicframework.com/docs/angular/lifecycle */
  ionViewWillEnter() {
    // TODO: can we simplify this to use only PlacesService and only in ngOnInit?
    this.view = 'loading-data';
    this.placesService.getPlaces().toPromise().then(places => {
      this.placesToDiscover = places;
      this.filterPlaces(this.currentFilter);
    }).finally(() => {
      this.view = this.placesToDiscover && this.placesToDiscover.length ? 'data-loaded' : 'no-data';
    });
  }

  showPlaceDetails(place: Place) {
    this.navConteoller.navigateForward(this.linksService.getPlaceDetailsLink(place.id));
  }

  filterPlaces(filter: PlaceFilter) {
    let places: Place[];
    switch (filter) {
      case 'all':
        places = [...this.placesToDiscover];
        break;
      case 'bookable':
        places = this.placesToDiscover.filter(place => this.placesService.canBook(place));
        break;
      default:
        throw new Error('INVALID_PLACES_FILTER');
    }
    this.primaryPlace = places[0];
    this.secondaryPlaces = places.slice(1);
  }
}
