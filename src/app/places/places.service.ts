import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { AuthService } from 'src/app/auth/services/auth.service';
import { PlacesApiService } from 'src/app/api/places-api.service';
import { ImageService } from 'src/app/api/image.service';
import { Place } from './place.model';
import { PlaceLocation } from './../shared/models/place-location.model';

@Injectable({
  providedIn: 'root'
})
export class PlacesService {
  // private placesDB: Place[] = [];
  // private placesObservable = new BehaviorSubject(this.placesDB); // [... this.placesDB];

  constructor(
    private authService: AuthService,
    private imageService: ImageService,
    private apiService: PlacesApiService
  ) { }

  // TODO: cache data?

  getPlaces(): Observable<Place[]> {
    return this.apiService.getPlaces();
  }
  getUserPlaces() {
    if (!this.authService.authenticated) {
      throw new Error('PLACES_USER_UNAUTHORIZED');
    }
    return this.apiService.getPlaces().pipe(
      map(places => {
        return places.filter(place => place.userId === this.authService.userID);
      })
    );
  }
  getPlace(id: string): Observable<Place> {
    return this.apiService.getPlace(id);
  }
  addPlace(title: string,
    description: string,
    price: number,
    availableFrom: Date,
    availableTo: Date,
    location: PlaceLocation,
    image: any): Promise<Place> {
    return new Promise((resolve, reject) => {
      this.imageService.uploadImage(this.imageService.convertImageStringToFile(image))
        .then(res => {
          const newPlace: Place = {
            title: title,
            description: description,
            imageUrl: res.imageUrl,
            price: price,
            availableFrom: availableFrom,
            availableTo: availableTo,
            userId: this.authService.userID,
            location: location
          };
          return newPlace;
        })
        .then(newPlace => {
          return this.apiService.createPlace(newPlace);
        })
        .then(createdPlace => {
          resolve(createdPlace);
        })
        .catch(err => {
          reject(err);
        });
    });
  }
  async updatePlace(id: string, title: string, description: string, price: number): Promise<Place> {
    const placeToEdit = await this.apiService.getPlace(id).toPromise();
    placeToEdit.title = title;
    placeToEdit.description = description;
    placeToEdit.price = price;
    return this.apiService.updatePlace(id, placeToEdit);
  }

  canBook(place: Place) {
    return this.authService.authenticated && place.userId !== this.authService.userID;
  }
}
