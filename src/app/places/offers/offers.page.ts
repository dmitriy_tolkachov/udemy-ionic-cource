import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { LinksService } from './../../core/links.service';
import { PlacesService } from './../places.service';
import { Place } from '../place.model';

type View = 'data-loaded' | 'no-data' | 'loading-data';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.page.html',
  styleUrls: ['./offers.page.scss'],
})
export class OffersPage implements OnInit, OnDestroy {
  offers: Place[];
  newOfferLink: string;
  view: View;

  private offers$: Subscription;

  constructor(
    private placesService: PlacesService,
    private linksService: LinksService
  ) { }

  ngOnInit() {
    this.offers$ = this.placesService.getUserPlaces().subscribe(offers => this.offers = offers);
    this.newOfferLink = this.linksService.getNewOfferLink();
  }
  ngOnDestroy() {
    if (this.offers$) {
      this.offers$.unsubscribe();
    }
  }
  // TODO: implement as on bookings page
  /** https://ionicframework.com/docs/angular/lifecycle */
  ionViewWillEnter() {
    this.view = 'loading-data';
    this.placesService.getUserPlaces().toPromise().then(places => {
      this.offers = places;
    }).finally(() => {
      this.view = this.offers && this.offers.length ? 'data-loaded' : 'no-data';
    });
  }

  getEditOfferLink(id: string) {
    return this.linksService.getEditOfferLink(id);
  }
}
