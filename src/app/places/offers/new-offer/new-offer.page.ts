import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NavController, LoadingController } from '@ionic/angular';

import { PlaceLocation } from 'src/app/shared/models/place-location.model';
import { LinksService } from 'src/app/core/links.service';
import { PlacesService } from 'src/app/places/places.service';

@Component({
  selector: 'app-new-offer',
  templateUrl: './new-offer.page.html',
  styleUrls: ['./new-offer.page.scss'],
})
export class NewOfferPage implements OnInit {
  private loadingOverlay: HTMLIonLoadingElement;

  defaultBackUrl: string;
  form: FormGroup;

  constructor(
    private linksService: LinksService,
    private navController: NavController,
    private placesService: PlacesService,
    private loadingController: LoadingController
  ) { }

  ngOnInit() {
    this.defaultBackUrl = this.linksService.getOffersLink();
    this.initForm();
  }

  private initForm() {
    this.form = new FormGroup({
      title: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.minLength(2), Validators.maxLength(100)]
      }),
      description: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.minLength(10), Validators.maxLength(256)]
      }),
      price: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.min(1)]
      }),
      availableFrom: new FormControl(null, {
        updateOn: 'blur',
        // validators:[Validators.]
      }),
      availableTo: new FormControl(null, {
        updateOn: 'blur',
        validators: []
      }),
      location: new FormControl(null, {
        validators: [Validators.required]
      }),
      image: new FormControl(null, {
        validators: [Validators.required]
      })
    });
  }
  private async showLoadingOverlay() {
    this.loadingOverlay = await this.loadingController.create({
      keyboardClose: true,
      message: 'Please wait...'
    });
    this.loadingOverlay.present();
  }
  private hideLoadingOverlay() {
    this.loadingOverlay.dismiss();
  }

  // TODO: move to a service
  isFormControlInvalid(formControlName: string) {
    const formControl = this.form.get(formControlName);
    return formControl.invalid && formControl.touched;
  }
  async createOffer() {
    if (this.form.invalid) {
      return;
    }
    await this.showLoadingOverlay();
    this.placesService.addPlace(
      this.form.value.title,
      this.form.value.description,
      +this.form.value.price,
      new Date(this.form.value.availableFrom),
      new Date(this.form.value.availableTo),
      this.form.value.location,
      this.form.value.image
    ).then(res => {
      return this.navController.navigateBack(this.linksService.getOffersLink());
    }).finally(() => {
      this.hideLoadingOverlay();
    });

  }

  pickLocation(location: PlaceLocation) {
    this.form.patchValue({ location: location });
  }

  pickImage(img: string) {
    this.form.patchValue({ image: img });
  }
}
