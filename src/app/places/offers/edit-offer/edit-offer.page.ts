import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NavController, LoadingController, AlertController } from '@ionic/angular';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { LinksService } from 'src/app/core/links.service';
import { PlacesService } from 'src/app/places/places.service';
import { Place } from '../../place.model';

type View = 'loading-data' | 'data-loaded';

@Component({
  selector: 'app-edit-offer',
  templateUrl: './edit-offer.page.html',
  styleUrls: ['./edit-offer.page.scss'],
})
export class EditOfferPage implements OnInit, OnDestroy {
  private paramsSubscription: Subscription;
  private placeSubscription: Subscription;
  private readonly OFFER_ID_PARAM = 'placeId';
  private loadingOverlay: HTMLIonLoadingElement;

  view: View;
  place: Place;
  form: FormGroup;
  defaultBackUrl: string;

  constructor(
    private placesService: PlacesService,
    private linksService: LinksService,
    private activatedRoute: ActivatedRoute,
    private navController: NavController,
    private loadingController: LoadingController,
    private alertController: AlertController
  ) { }

  ngOnInit() {
    this.view = 'loading-data';
    this.defaultBackUrl = this.linksService.getOffersLink();
    this.paramsSubscription = this.activatedRoute.paramMap.subscribe(params => {
      if (params.has(this.OFFER_ID_PARAM)) {
        this.placeSubscription = this.placesService.getPlace(params.get(this.OFFER_ID_PARAM))
          .subscribe(
            place => {
              this.place = place;
              this.initForm();
              this.view = 'data-loaded';
            },
            error => { this.showCriticalError(`Can't find such place.`); }
          );
      } else {
        this.navController.navigateBack(this.linksService.getOffersLink());
      }
    });
  }
  ngOnDestroy() {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
    if (this.placeSubscription) {
      this.placeSubscription.unsubscribe();
    }
  }

  private initForm() {
    this.form = new FormGroup({
      title: new FormControl(this.place.title, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.minLength(2), Validators.maxLength(100)]
      }),
      description: new FormControl(this.place.description, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.minLength(10), Validators.maxLength(256)]
      }),
      price: new FormControl(this.place.price, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.min(1)]
      }),
      // availableFrom: new FormControl(null, {
      //   updateOn: 'blur',
      //   // validators:[Validators.]
      // }),
      // availableTo: new FormControl(null, {
      //   updateOn: 'blur',
      //   validators: []
      // })
    });
  }
  private async showLoadingOverlay() {
    this.loadingOverlay = await this.loadingController.create({
      keyboardClose: true,
      message: 'Please wait...'
    });
    this.loadingOverlay.present();
  }
  private hideLoadingOverlay() {
    this.loadingOverlay.dismiss();
  }
  private showCriticalError(msg: string) {
    return this.alertController.create({
      header: 'Error',
      message: msg || 'Critical error has occured.',
      buttons: [{
        text: 'Got it',
        handler: () => {
          this.navController.navigateBack(this.linksService.getOffersLink());
        }
      }]
    }).then(el => el.present());
  }

  // TODO: move to a service
  isFormControlInvalid(formControlName: string) {
    const formControl = this.form.get(formControlName);
    return formControl.invalid && formControl.touched;
  }
  async updateOffer() {
    if (this.form.invalid) {
      return;
    }

    await this.showLoadingOverlay();
    await this.placesService.updatePlace(
      this.place.id,
      this.form.value.title,
      this.form.value.description,
      +this.form.value.price
    );
    this.hideLoadingOverlay();

    return this.navController.navigateBack(this.linksService.getOffersLink());
  }
}
