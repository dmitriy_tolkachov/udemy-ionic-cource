import { PlaceLocation } from './../shared/models/place-location.model';

export class Place {
    constructor(
        public title: string,
        public description: string,
        public imageUrl: string,
        public price: number,
        public availableFrom: Date,
        public availableTo: Date,
        public userId: string,
        public location: PlaceLocation,
        /** if empty - place is not saved in DB */
        public id?: string
    ) { }
}
