// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  firebaseDbUrl: 'https://udemy-ionic-cource.firebaseio.com/',
  firebaseFunctions: 'https://us-central1-udemy-ionic-cource.cloudfunctions.net',
  firebaseAuthAPI: 'https://identitytoolkit.googleapis.com/',
  firebaseKey: 'AIzaSyAVJkzSy9W-JX7yWvWg_ors5ymhAw-XyXM',
  googleMapsKey: 'AIzaSyDd_TClwZCtmtZ4TSFmfXKmS7OMeo9bgD0'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
